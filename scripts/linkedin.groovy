@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.4')
@Grab(group='javax.mail', module='mail', version='1.4.7')

import static groovyx.net.http.MultipartContent.multipart
import static groovyx.net.http.HttpBuilder.configure
import static groovyx.net.http.ContentTypes.JSON
import groovyx.net.http.CoreEncoders
import groovyx.net.http.*
import static java.util.Calendar.*

year = args.length > 0 ? args[0] as int : new Date()[YEAR]
month = args.length > 1 ? args[1] as int : new Date()[MONTH]+1
day = args.length > 2 ? args[2] as int : new Date()[DAY_OF_MONTH]

println "Processing $year/$month/$day"

author=System.getenv("LINKEDIN_USER")
accessToken=System.getenv("LINKEDIN_TOKEN")

if( !author || !accessToken ){
    println "Necesito la configuracion de linkedin"
    return
}

http = configure{
    request.uri = "https://api.linkedin.com"
    request.contentType = JSON[0]
    request.headers['Authorization'] = "Bearer $accessToken"
    request.headers['LinkedIn-Version'] = "202308"
    request.headers['X-Restli-Protocol-Version'] = '2.0.0'
}

html = ""
img = ""
title = ""
body = ""

['en':'' ].each{ kv ->
    String lang = kv.key 
    String emoji = kv.value
    String[]found

    if( new File("source/csv/${year}/${lang}.tsv").exists() ){
        new File("source/csv/${year}/${lang}.tsv").withReader{ reader ->
            reader.readLine()
            String line
            while( (line=reader.readLine()) != null){
                def fields = line.split('\t')
                if( fields.length != 5)
                    continue
                if( fields[0] as int == day && fields[1] as int == month && fields[2] as int == year){
                    found = fields
                    break
                }
            }
        }
    }

    if(!found){
        println "not found $year/$month/$day"
        return
    }

    title=  found[4].split('\\.').first()
    body=  found[4].split('\\.').drop(1).join('\n')

    if( !img ){
        img = "https://calendario-cientifico-escolar.gitlab.io/_/images/${year}/${found[3]}.jpg"
    }

    html +="""$title

$body
"""
}


html += """

Source: Calendario Cientifico Escolar, https://t.me/CalendarioCientifico
"""

eventYear = title.split(' ').last() as int
html = "${year-eventYear} years ago, $html"

json = http.post{
    request.uri.path = "/rest/images"
    request.uri.query = ["action":"initializeUpload"]
    request.body = [
	 initializeUploadRequest: [
		"owner": "urn:li:person:"+author,
	        "altText": findAltText(year, month, day)
	 ]
    ]
}

uploadUrl = json.value.uploadUrl
mediaId = json.value.image

someFile = File.createTempFile("img","jpg")
someFile.bytes = img.toURL().bytes

configure{
    request.uri = uploadUrl
    request.contentType = 'image/jpg'
    request.headers['Authorization'] = "Bearer $accessToken"
    request.headers['X-Restli-Protocol-Version'] = '2.0.0'
    request.headers['Accept']='*/*'    
    request.headers['LinkedIn-Version'] = "202308"
    request.body = someFile
    request.encoder('image/jpg'){ ChainedHttpConfig config, ToServer req->
        req.toServer(new ByteArrayInputStream(someFile.bytes))
    }    
}.put()


articulo = [
    "author": "urn:li:person:"+author,
    "commentary": html,
    "visibility": "PUBLIC",
    "distribution": [
      "feedDistribution": "MAIN_FEED",
      "targetEntities": [],
      "thirdPartyDistributionChannels": []
    ],
    "lifecycleState": "PUBLISHED",
    "isReshareDisabledByAuthor": false,
    "content":[
	"media": ["id":mediaId]
    ]
]
resp = http.post{
    request.uri.path = "/rest/posts"
    request.body = articulo
}



String findAltText(year,month, day){
    def tags="No tenemos disponible una descripción de esta imagen pero estamos trabajando en ello";
    def file = new File("source/csv/${year}/alttext.csv")
    if (file.exists()){
        def lines = file.text.lines()
        def line = lines.each{ l->
            def fields = l.split(";")
            if( fields.length != 3)
                return false
            if( fields[0] as int == day && fields[1] == month ){
                tags = fields[2]
            }
        }
    }
    return tags;
}
