= Un anniversaire scientifique pour chaque jour de l'année.

Le Calendrier Scientifique Scolaire s'adresse principalement aux élèves de l'école primaire et secondaire. Chaque jour, un anniversaire scientifique ou technologique est proposé, par exemple les naissances de personnes de ces disciplines ou les commémorations de découvertes remarquables.

Le calendrier est accompagné d'un guide didactique avec des orientations pour son utilisation pédagogique transversale en classe. Ces propositions didactiques reposent sur les principes d'inclusion, de standardisation et d'équité et sont accompagnées de lignes directrices générales sur l'accessibilité. Pour cela, des tâches variées sont proposées qui incluent un large éventail de compétences et de niveaux de difficulté et qui, développées en coopération, permettent à tous les élèves d'apporter des contributions utiles et pertinentes.

Pour favoriser sa diffusion et son utilisation en classe, tout le matériel est traduit en plusieurs langues : espagnol, galicien, basque, catalan, asturien, aragonais, anglais, français, esperanto et arabe.

Les informations avec les anniversaires quotidiens et les illustrations qui l'accompagnent sont disponibles gratuitement sur ce site. De plus, le calendrier et le guide (mise en page au format pdf et texte brut) peuvent être téléchargés sur le site Web de l'IGM (http://www.igm.ule-csic.es/calendario-cientifico).

Cette initiative vise à contribuer à rapprocher la culture scientifique des plus jeunes et à créer pour eux des références les plus proches possibles. Pour cette raison, un plus grand effort a été fait pour faire connaître les personnes et les découvertes du présent qui constituent des références pour les jeunes et, en même temps, donnent une vision de dynamisme et d'actualité. Une attention particulière a été accordée à la promotion d'un langage non sexiste et à l'augmentation de la visibilité des femmes scientifiques et technologues, pour mettre à disposition des modèles de référence qui promeuvent les vocations scientifiques et techniques chez les filles et les adolescentes. 


== Twitter

@CalCientifico

== Telegram

https://t.me/CalendarioCientifico

== iCal

link:{attachmentsdir}/fra.ical[Download]
